(function ($, H) {

  var ajax_url = {},
    chart_container = {};
  Drupal.PlaygroundCharts = Drupal.PlaygroundCharts || {};
  Drupal.PlaygroundCharts.Drilldown = Drupal.PlaygroundCharts.Drilldown || {};

  /**
   * Load remote content after the main page loaded.
   */
  Drupal.behaviors.playgroundChartsDrilldownAjax = {
    attach: function(context, settings) {
      ajax_url = {
        path: settings.playground_charts.url_callback,
        suffix: settings.playground_charts.url_suffix,
      };
      chart_container = $('.charts-highchart', context);
      chart_container.once('charts-highchart', function() {
        if ($(this).attr('data-chart')) {
          var config = $.parseJSON($(this).attr('data-chart'));
          // https://jsfiddle.net/gh/get/library/pure/highcharts/highcharts/tree/master/samples/highcharts/drilldown/drillupbutton/
          H.setOptions({
            lang: {
              drillUpText: '◁ Go back'
            }
          });
          if ($(this).hasClass('charts-highchart--async-drilldown')) {
            config.chart.events = { drilldown: Drupal.PlaygroundCharts.Drilldown };
          }
          $(this).highcharts(config);
        }
      });
    }
  };

  Drupal.PlaygroundCharts.Drilldown = function (e) {
    if (!e.seriesOptions && !chart_container.hasClass('drilling-down')) {
      var thing = e.point.name,
        color = 'all',
        allThing = true,
        point = e.point;

      // User clicked one of the stacked color
      if (e.originalEvent.hasOwnProperty('point')) {
        point =  e.originalEvent.point;
        thing = point.name;
        color = point.color_id;
        allThing = false;
      }
      Drupal.PlaygroundCharts.UpdateGraph(this, thing, color, point, allThing);
      // To prevent the drill down event running down more that once.
      chart_container.addClass('drilling-down');
    }
  };

  Drupal.PlaygroundCharts.UpdateGraph = function(chart, thing, color, point, allThing) {
    // Drupal ajax element settings.
    var base = 'highchart-render',
      element_settings = {
        url: ajax_url.path + thing + '/' + color + ajax_url.suffix,
        event: '',
        progress: {
          type: 'throbber'
        },
        wrapper: base
      };

    Drupal.ajax[base] = new Drupal.ajax(base, chart_container, element_settings);
    // On Drupal ajax success callback.
    Drupal.ajax[base].options.success = function (response, status) {
      if (typeof response == 'string') {
        response = $.parseJSON(response);
      }
      // Call the ajax success method.
      Drupal.ajax[base].success(response, status);

      // Show the loading message
      chart.showLoading('Loading data ...');

      // Updating the drilldown object.
      chart.hideLoading();
      if (!allThing) {
        chart.addSeriesAsDrilldown(point, response[1].settings.playground_charts.drilldown);
      }
      else {
        var i,
          series = response[1].settings.playground_charts.drilldown;
        for (i = 0; i < series.length; i++) {
          chart.addSingleSeriesAsDrilldown(point, series[i]);
        }
        chart.applyDrilldown();
      }
    };

    // Triggering the ajax.
    Drupal.ajax[base].eventResponse(Drupal.ajax[base].element, '');
  };

  Highcharts.Chart.prototype.callbacks.push(function (chart) {
    // This event get triggered after the drilldown, removing the drilling-down
    // class here.
    H.addEvent(chart.container, 'click', function (e) {
      if (chart_container.hasClass('drilling-down')) {
        chart_container.removeClass('drilling-down');
      }
    });
  });
})(jQuery, Highcharts);
